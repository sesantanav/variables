﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persona
{
    /// <summary>
    /// Clase información generica sobre una persona
    /// Adicional
    /// </summary>
    class Persona
    {
        string nombre;

        int a = 0;

        /// <summary>
        /// Método para definir el valor del atributo nombre
        /// </summary>
        /// <param name="_nombre">Es el nombre</param>
        public void setNombre(string _nombre)
        {
            this.nombre = _nombre;
        }

        /// <summary>
        /// Método para obtener el nombre de la persona
        /// </summary>
        /// <returns></returns>
        public string getNombre()
        {
            /**
             * int      4 bytes
             * long     8 bytes
             * float    4 bytes
             * double   8 bytes
             * bool     1 bit
             * char     2 bytes
             * string   2 bytes por caracter
             */
            string caracteres = "aaaaa";

            return this.nombre;
        }

    }

}