﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Persona;

namespace Ejemplo1
{
    /// <summary>
    /// Programa ejemplo definición de variables con C#
    /// </summary>
    class Program
    {
        int resultado = 1;

        static void Main(string[] args)
        {
            // declaración de variables
            int a = 0;
            int b = 1;

            double d = 3.14;
            string mensaje = "Hola Mundo!!";
            char caracter = 'A';
            bool verdadero = true; // false

            // constantes
            const double PI = 3.1416;   //

            System.Console.WriteLine(mensaje + " " + caracter);

            int resultado = 0;
            int c = 2;

            System.Console.WriteLine("El resultado es: " + resultado);

            resultado = Suma(a, c);

            System.Console.WriteLine(resultado);

            resultado = Multi(a, b);

            System.Console.WriteLine(resultado);

            // double PI = 3.14;

            Persona.Persona sebastian = new Persona.Persona();

            sebastian.setNombre("Sebastián");
            string nombre = sebastian.getNombre();
            System.Console.WriteLine("El nombre de la persona es: " + nombre);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static int Suma(int a, int b)
        {
            int resultado = 0;
            resultado =  a + b;
            return resultado;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static int Multi(int a, int b)
        {
            int resultado = 0;
            resultado = a * b;
            return resultado;
        }

        public static int Informacion()
        {
            
            /**
             * 
             * 
             */


            return 0;
        }
    }
}
